package challenge

import (
	"reflect"
	"testing"
)

func TestIndexSelect1(t *testing.T) {
	want := [][]int{{1, 1, 3}}
	msg := IndexSelect([][]int{{1, 2, 3, 4}}, 0, []int{0, 0, 2})
	if !reflect.DeepEqual(want, msg) {
		t.Fatalf(`%q, not match with expected %#q`, msg, want)
	}
}

func TestIndexSelect2(t *testing.T) {
	want := [][]int{{1, 2}}
	msg := IndexSelect([][]int{{1, 2}, {3, 4}}, 0, []int{0})
	if !reflect.DeepEqual(want, msg) {
		t.Fatalf(`%q, not match with expected %#q`, msg, want)
	}
}

func TestIndexSelect3(t *testing.T) {
	want := [][]int{{1, 2}, {1, 2}}
	msg := IndexSelect([][]int{{1, 2}, {3, 4}}, 0, []int{0, 0})
	if !reflect.DeepEqual(want, msg) {
		t.Fatalf(`%q, not match with expected %#q`, msg, want)
	}
}

func TestIndexSelect4(t *testing.T) {
	want := [][]int{{1, 2}, {1, 2}, {3, 4}, {3, 4}}
	msg := IndexSelect([][]int{{1, 2}, {3, 4}}, 0, []int{0, 0, 1, 1})
	if !reflect.DeepEqual(want, msg) {
		t.Fatalf(`%q, not match with expected %#q`, msg, want)
	}
}

func TestIndexSelect5(t *testing.T) {
	want := [][]int{{1}, {3}}
	msg := IndexSelect([][]int{{1, 2}, {3, 4}}, 1, []int{0})
	if !reflect.DeepEqual(want, msg) {
		t.Fatalf(`%q, not match with expected %#q`, msg, want)
	}
}

func TestIndexSelect6(t *testing.T) {
	want := [][]int{{1, 1}, {3, 3}}
	msg := IndexSelect([][]int{{1, 2}, {3, 4}}, 1, []int{0, 0})
	if !reflect.DeepEqual(want, msg) {
		t.Fatalf(`%q, not match with expected %#q`, msg, want)
	}
}

func TestIndexSelect7(t *testing.T) {
	want := [][]int{{1, 1, 2, 2}, {3, 3, 4, 4}}
	msg := IndexSelect([][]int{{1, 2}, {3, 4}}, 1, []int{0, 0, 1, 1})
	if !reflect.DeepEqual(want, msg) {
		t.Fatalf(`%q, not match with expected %#q`, msg, want)
	}
}
