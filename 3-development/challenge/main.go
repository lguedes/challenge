package challenge

func IndexSelect(input [][]int, layer int, index []int) [][]int {
	r := makeReturnArray(len(input), len(index), layer)

	if layer == 0 {
		if len(input) == 1 {
			for i, v := range index {
				r[0][i] = input[0][v]
			}
		} else {
			for i, v := range index {
				r[i] = input[v]
			}
		}
	} else {
		for i, v := range index {
			for j := range input {
				r[j][i] = input[j][v]
			}
		}
	}

	return r
}

func makeReturnArray(inputsize int, indexsize int, layer int) [][]int {
	var row, col int
	row = inputsize
	col = indexsize

	if layer == 0 && inputsize > 1 {
		row = indexsize
		col = inputsize
	}

	r := make([][]int, row)
	for i := range r {
		r[i] = make([]int, col)
	}
	return r
}
