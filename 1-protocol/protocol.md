## Secure Exchange Files Protocol
Author: Lucas Guedes Reis \<lguedes@gmail.com\>

The proposed protocol is designed to ensure the secure exchange of encrypted files between our Mobile SDK and an HTTPS backend while functioning as an additional security layer over HTTPS.

### Flow:

![Flow](protocol.png)

1. **Client-Server Handshake**

   - The client initiates a connection to the HTTPS backend using the standard TLS handshake.
   - A secure channel is established between the client and server, upon successful completion of the TLS handshake.

2. **File Transfer Request**

   - The client sends a request to the backend for file transfer, specifying the file's metadata and an encrypted key for secure communication. This key is protected using the client's public key.

3. **File Encryption on the Server**

   - The backend server generates a random Data Encryption Key (DEK) for the file to be transferred. The DEK is used for symmetric encryption.
   - The file is encrypted using a symmetric encryption 256-bit AES algorithm, with the DEK.
   - The DEK is then encrypted with the public key of the client, ensuring only the client can decrypt the DEK.

4. **Encrypted File Transfer**

   - The encrypted file, along with the encrypted DEK and metadata, is transmitted over the established TLS channel from the server to the client.

5. **Client Decryption Process**

   - The client receives the encrypted data.
   - It decrypts the DEK using the client private key.
   - The DEK is then used to decrypt the file content.
   - Use SHA-256 for file integrity verification (Hash is part of metadata).

6. **Close Connection**

   - Upon successful file transfer and decryption, close the connection.

### Security Considerations:

- Client uses a 2048-bit RSA key pair
- Regularly update encryption keys and certificates.
- Ensure that the client has a secure and tamper-resistant key storage mechanism for its private key.

This protocol enhances the security of file exchange over HTTPS by adding an additional encryption layer. Actual implementation would require careful consideration of various factors, but this protocol provides a robust foundation for secure file exchange. 

You can enhance the security of this protocol adding a Key Encryption Key (KEK) strategy, but it will add more components and complexity to the protocol. We need more specific requirements to define the best approach.

