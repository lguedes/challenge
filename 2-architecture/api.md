# API Documentation 
Author: Lucas Guedes Reis \<lguedes@gmail.com\>

## API Flow

![Flow](API.png)

## **Components**

1. **API**

Exposing endpoints through an API Gateway is a common practice in modern application development, especially in microservices architecture. An API Gateway is a centralized entry point that acts as a reverse proxy to route and manage HTTP requests to various microservices, APIs, or backend services. 
It provide a layer of security by handling tasks like authentication, authorization, and rate limiting.

An API Gateway also can handle some transformations in JSON or XML, but, in our case, we need a more sofisticated approach to solve the captcha and extract the necessary parameters to do the qualification

For this API, we are considering token authentication, because it's is a B2B integration.The API key should be included in the request header as "X-API-Key." 

2. **Scraper**

Scraping a banking page with a CAPTCHA on authentication can be a complex and challenging task. We will create a specific component, for that purpose.
After the authentication process (handle user session), this component can call the pages containing data, extract the relevant data from the HTML, and do the credit qualification process.

## Open API Specification

[Open API Schema](schema.json)

![Schema Image](schema.png)

